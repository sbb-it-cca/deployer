FROM docker:18

ARG KUBECTL_VERSION="v1.13.3"
ARG HELM_VERSION="v2.13.0-rc.2"
ARG UID=1500
ARG GID=1500

LABEL maintainer="Simon Erhardt <simon.erhardt@sbb.ch>"

# Update
RUN apk -U --no-cache upgrade

# Install
RUN apk -U --no-cache add \
  curl \
  bash \
  tar \
  tini

# Install kubectl
RUN curl https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl -s \
  && chmod +x /usr/local/bin/kubectl

# Install helm
RUN curl https://storage.googleapis.com/kubernetes-helm/helm-${HELM_VERSION}-linux-amd64.tar.gz -o /tmp/helm.tgz -s \
  && mkdir /tmp/helm \
  && tar -xvzf /tmp/helm.tgz --strip-components=1 -C /tmp/helm \
  && mv /tmp/helm/helm /usr/local/bin \
  && mv /tmp/helm/tiller /usr/local/bin \
  && rm -rf /tmp/helm.tgz /tmp/helm

ENTRYPOINT ["/sbin/tini","--"]
CMD ["/bin/bash"]